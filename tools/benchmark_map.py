
import python_utils.evaluate_detection as eval
import python_utils.general_utils as g_utils
import datasets
import os
from datasets.imdb import imdb
import xml.dom.minidom as minidom
import numpy as np
import scipy.sparse
import scipy.io as sio
import utils.cython_bbox
import cPickle
import subprocess
from IPython.core.debugger import Tracer

def benchmark_map(classes, num_images, all_boxes, gt_roidb, output_dir, det_salt = '', eval_salt = '', overlap_thresh = 0.5):
      
      # NOTE: classes should always have "__Background__" as the first element

      num_classes = len(classes)
      class_to_ind = dict(zip(classes, range(classes)))


      # gt_roidb is a list of dictionary. Let's write the format down in JSON format: (Note: x1 < x2, y1 < y2)
      #
      # [
      #   {
      #     'gt_classes': <list of detected classes. length n should be same as the number of boxes>,
      #     'boxes': <2D numpy array of boxes (size = (n, 4)); in any row, the format shoule be [x1,y1,x2,y2] >
      #   },
      #   {
      #    ...
      #   },
      #   ...
      # ]

      gt_roidb = self.gt_roidb()

      # all_boxes
      # all_boxes[i][j] means the boxes information for category i on frame j. It can be None or a 2D numpy array
      # where the array should be in this format:
      # [[x1,y1,x2,y2,score], [......], [....], ....]
      #
      # Note: x1 < x2, y1 < y2

      ap = [[]]; prec = [[]]; rec = [[]]
      ap_file = os.path.join(output_dir, 'eval' + det_salt + eval_salt + '.txt')
      with open(ap_file, 'wt') as f:
          for i in xrange(1, num_classes):
              dt = []; gt = [];
              # Prepare the output
              for j in xrange(0,num_images):
                  bs = all_boxes[i][j]
                  if len(bs) == 0:
                    bb = np.zeros((0,4)).astype(np.float32)
                    sc = np.zeros((0,1)).astype(np.float32)
                  else:
                    bb = bs[:,:4].reshape(bs.shape[0],4)
                    sc = bs[:,4].reshape(bs.shape[0],1)
                  dtI = dict({'sc': sc, 'boxInfo': bb})
                  dt.append(dtI)
          
              # Prepare the annotations
              for j in xrange(0,num_images):
                  cls_ind = np.where(gt_roidb[j]['gt_classes'] == i)[0]
                  bb = gt_roidb[j]['boxes'][cls_ind,:]
                  diff = np.zeros((len(cls_ind),1)).astype(np.bool)
                  gt.append(dict({'diff': diff, 'boxInfo': bb}))
              bOpts = dict({'minoverlap': overlap_thresh})
              ap_i, rec_i, prec_i = eval.inst_bench(dt, gt, bOpts)[:3]
              ap.append(ap_i[0]); prec.append(prec_i); rec.append(rec_i)
              ap_str = '{:20s}: ap   {:10f}'.format(classes[i], ap_i[0]*100)
              f.write(ap_str + '\n')
              print ap_str
          # ap_str = '{:20s}:{:10f}'.format('mean', np.mean(ap[1:])*100)
          ap_str = '{:20s}:{:10f}'.format('mean', np.mean([item for item in ap[1:] if not np.isnan(item)])*100)
          f.write(ap_str + '\n')
          print ap_str

      # eval_file = os.path.join(output_dir, 'eval' + det_salt + eval_salt + '.pkl')
      # g_utils.save_variables(eval_file, [ap, prec, rec, classes, class_to_ind], \
      #     ['ap', 'prec', 'rec', 'classes', 'class_to_ind'], overwrite = True)
      # eval_file = os.path.join(output_dir, 'eval' + det_salt + eval_salt + '.mat')
      # g_utils.scio.savemat(eval_file, {'ap': ap, 'prec': prec, 'rec': rec, 'classes': classes}, do_compression = True);
      
      return ap, prec, rec, classes, class_to_ind