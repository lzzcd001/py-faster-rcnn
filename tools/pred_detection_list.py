import _init_paths
from fast_rcnn.config import cfg
from fast_rcnn.test import im_detect
from fast_rcnn.nms_wrapper import nms
from utils.timer import Timer
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse

def vis_detections(im, class_name, dets, thresh=0.5):
    """Draw detected bounding boxes."""
    inds = np.where(dets[:, -1] >= thresh)[0]
    if len(inds) == 0:
        return

    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect='equal')
    for i in inds:
        bbox = dets[i, :4]
        score = dets[i, -1]

        ax.add_patch(
            plt.Rectangle((bbox[0], bbox[1]),
                          bbox[2] - bbox[0],
                          bbox[3] - bbox[1], fill=False,
                          edgecolor='red', linewidth=3.5)
            )
        ax.text(bbox[0], bbox[1] - 2,
                '{:s} {:.3f}'.format(class_name, score),
                bbox=dict(facecolor='blue', alpha=0.5),
                fontsize=14, color='white')

    ax.set_title(('{} detections with '
                  'p({} | box) >= {:.1f}').format(class_name, class_name,
                                                  thresh),
                  fontsize=14)
    plt.axis('off')
    plt.tight_layout()
    plt.draw()

def pred_single_image(net, img_path, class_list, thresh = 0.4):
    num_classes = len(class_list)

    print img_path
    # format (x1, y1, x2, y2, score)

    pred_res = []
    box_proposals = None
    all_boxes = []
    all_boxes.append([]); # for background

    im = cv2.imread(img_path)
    scores, boxes = im_detect(net, im, box_proposals)

    # skip j = 0, because it's the background class
    CONF_THRESH = 0.8
    NMS_THRESH = 0.3
    for cls_ind, cls in enumerate(class_list[1:]):
        cls_ind += 1 # because we skipped background
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        dets = np.hstack((cls_boxes,
                          cls_scores[:, np.newaxis])).astype(np.float32)
        keep = nms(dets, NMS_THRESH)
        dets = dets[keep, :]
	all_boxes.append(dets);
        #vis_detections(im, cls, dets, thresh=CONF_THRESH)
    #plt.show()

    for pred_class_no in range(1, num_classes):       
	if all_boxes[pred_class_no] == []:
            continue
        for inst_no in range(all_boxes[pred_class_no].shape[0]):
            pred_class_name = class_list[pred_class_no]
            pred_prob = all_boxes[pred_class_no][inst_no, 4]
	    if pred_prob < CONF_THRESH:
		continue;
            pred_box = all_boxes[pred_class_no][inst_no, 0:4]
            # Note: prediction class number = pred_class_no
            output_pred = (pred_box[0], pred_box[1], pred_box[2], pred_box[3], pred_prob, pred_class_name.strip('\n'))
            pred_res.append(output_pred)
    return pred_res


def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Faster R-CNN Detection')
    parser.add_argument('--caffemodel', dest='caffemodel', help='Caffemodel to use', default='vgg16')
    parser.add_argument('--prototxt', dest='prototxt', help='Prototxt file', default='prototxt')
    parser.add_argument('--c_filelist', dest='filelist', help='List of images', default='list.txt')
    parser.add_argument('--c_classes', dest='classes', help='List of classes', default='classnames.txt')
    parser.add_argument('--dest', dest='dest', help='Destination file', default='/tmp/bboxes.txt')

    args = parser.parse_args()

    return args

if __name__ == '__main__':
    cfg.TEST.HAS_RPN = True  # Use RPN for proposals

    args = parse_args()

    prototxt = args.prototxt;
    caffemodel = args.caffemodel;

    if not os.path.isfile(caffemodel):
        raise IOError(('{:s} not found.\nDid you run ./data/script/'
                       'fetch_faster_rcnn_models.sh?').format(caffemodel))

    caffe.set_mode_gpu()

    # Class list
    classnames = [];
    clist = open(args.classes);
    classes = clist.readlines();
    for className in classes:
	classnames.append(className.strip('\n'));

    print classnames

    # Load the net
    net = caffe.Net(prototxt, caffemodel, caffe.TEST)

    # open the destination file
    destFile = open(args.dest, 'w');
    

    # Read image files	
    flist = open(args.filelist);
    files = flist.readlines();
    count=0;
    for image in files:
	image = image.strip('\n');
	predictions = pred_single_image(net, image, classes);
	for prediction in predictions:
		outputStr = str(count)+' '+str(int(prediction[0]))+' '+str(int(prediction[1]))+' '+str(int(prediction[2]))+' '+str(int(prediction[3]))+' '+str(prediction[4])+' '+str(prediction[5])+'\n';
		destFile.write(outputStr);
	count=count+1;
 

    flist.close();
    destFile.close();
		 
	


