function edgeboxes_scene(dataset_path, save_path)
    
    raw_scene_paths = dir(fullfile(dataset_path, 'imgs'));
    raw_scene_paths = raw_scene_paths(3:end);
    raw_scene_paths = {raw_scene_paths(:).name};
    for raw_scene_path = raw_scene_paths
        scene_path = fullfile(dataset_path, 'imgs', raw_scene_path{1})
        if strcmp(scene_path(end-3:end), '_rgb')
            continue
        end
        filelist = dir(scene_path);
        filelist = filelist(3:end);
        save_scene_path = fullfile(save_path, raw_scene_path{1});
        if ~exist(save_scene_path)
            mkdir(save_scene_path)
        end
        for jj = 1:length(filelist)
            raw_filename_cell = filelist(jj);
            filename = raw_filename_cell.name;
            if raw_filename_cell.isdir == 1 || strcmp(filename(end-3:end), '.mat') || strcmp(filename(end-3:end), '.txt') || strcmp(filename(end-8:end), 'depth.png') 
                continue
            end
            filename_stem = filename(1:end-10);
            save_boxes_fname = fullfile(save_scene_path, sprintf('%s.txt', filename_stem));
            if exist(save_boxes_fname)
                continue
            end
            try
                img = imread(fullfile(scene_path, filename));
            catch
                fullfile(scene_path, filename)
                raise
            end
            model=load('/home/tom/edges/models/forest/modelBsds'); model=model.model;
            model.opts.sharpen=0;
%             model.opts.multiscale=0; model.opts.sharpen=2; model.opts.nThreads=4;

            %% set up opts for edgeBoxes (see edgeBoxes.m)
            opts = edgeBoxes;
            opts.alpha = .65;     % step size of sliding window search
            opts.beta  = .75;     % nms threshold for object proposals
            opts.minScore=.02;
            opts.maxBoxes = 200;  % max number of boxes to detect
            
            bboxs = edgeBoxes( img, model, opts );
            bboxs = bboxs(:, 1:4);
            bboxs(:,3) = bboxs(:,1) + bboxs(:,3);
            bboxs(:,4) = bboxs(:,2) + bboxs(:,4);
            fid = fopen(save_boxes_fname, 'w');
            for kk = 1:size(bboxs,1)
                fwrite(fid, sprintf('%f %f %f %f\n', bboxs(kk,1), bboxs(kk,2), bboxs(kk,3), bboxs(kk,4)));
            end
            fclose(fid);
        end
    end
end