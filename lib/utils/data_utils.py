import os

def get_immediate_files(a_dir, absolute=False):
	if absolute:
		return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
			if os.path.isfile(os.path.join(a_dir, name))]
	else:
		return [name for name in os.listdir(a_dir)
			if os.path.isfile(os.path.join(a_dir, name))]

def get_immediate_subdirectories(a_dir, absolute=False):
	return [name for name in os.listdir(a_dir)
		if os.path.isdir(os.path.join(a_dir, name))]

def exist_or_mkdir(a_dir):
	if not os.path.exists(a_dir):
		os.mkdir(a_dir)

def min_and_max(a_list):
	return (min(a_list), max(a_list))