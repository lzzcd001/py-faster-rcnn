#!/bin/bash
# Usage:
# ./experiments/scripts/faster_rcnn_end2end.sh GPU NET DATASET [options args to {train,test}_net.py]
# DATASET is either pascal_voc or coco.
#
# Example:
# ./experiments/scripts/faster_rcnn_end2end.sh 0 VGG_CNN_M_1024 pascal_voc \
#   --set EXP_DIR foobar RNG_SEED 42 TRAIN.SCALES "[400, 500, 600, 700]"

set -x
set -e

export PYTHONUNBUFFERED="True"

GPU_ID=$1
THRESH=$2
SAVEPATH=$3
NET=$4

PT_DIR="uw"

array=( $@ )
len=${#array[@]}
EXTRA_ARGS=${array[@]:4:$len}
EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}

NET_FINAL="/home/tom/object_slam_detection/py-faster-rcnn/output/uw_end2end/uw_rgb_train/vgg16_faster_rcnn_iter_70000.caffemodel"

time ./tools/uw_scene_extract_pred.py --gpu ${GPU_ID} \
  --def models/${PT_DIR}/${NET}/faster_rcnn_end2end/test.prototxt \
  --net ${NET_FINAL} \
  --cfg experiments/cfgs/faster_rcnn_end2end.yml \
  --thresh ${THRESH} \
  --save_path ${SAVEPATH} \
  ${EXTRA_ARGS}